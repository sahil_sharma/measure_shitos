#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <string.h>
#include <pthread.h>

#include <sys/timeb.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "governor_part1.h"

static inline void reset_counters()
{
    // Reset all cycle counter and event counters
    asm volatile ("mcr p15, 0, %0, c9, c12, 0\n\t" :: "r"(0x00000017));  
}

static inline void read_reg1(unsigned long long & count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000000));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (count)); 
}

static inline void read_reg2(unsigned long long & count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000001));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (count)); 
}

static inline void read_reg3(unsigned long long & count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000002));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (count)); 
}

static inline void read_reg4(unsigned long long & count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000003));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (count)); 
}

static inline void read_reg5(unsigned long long & count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000004));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (count)); 
}

static inline void read_reg6(unsigned long long & count)
{
    asm volatile ("mcr p15, 0, %0, c9, c12, 5\n\t" :: "r"(0x00000005));
    asm volatile("mrc p15, 0, %0, c9, c13, 2\n\t" : "=r" (count)); 
}


static inline void read_cpu_cycles(unsigned long long & count)
{
    asm volatile("mrc p15, 0, %0, c9, c13, 0\n\t" : "=r" (count)); 

}
